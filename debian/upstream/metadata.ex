# Example file for upstream/metadata.
# See https://wiki.debian.org/UpstreamMetadata for more info/fields.
# Below an example based on a github project.

# Bug-Database: https://github.com/<user>/rl-accel/issues
# Bug-Submit: https://github.com/<user>/rl-accel/issues/new
# Changelog: https://github.com/<user>/rl-accel/blob/master/CHANGES
# Documentation: https://github.com/<user>/rl-accel/wiki
# Repository-Browse: https://github.com/<user>/rl-accel
# Repository: https://github.com/<user>/rl-accel.git
