rl-accel (0.9.1-2) unstable; urgency=medium

  * removed the spurious build-dependency on libpython3.12-dev, which was
    left in d/control. Thanks for the custody and reopening of the bug report,
    Graham Inggs! Hopefully and definitely... Closes: #1094279
  * bumped Standards-Version: 4.7.2

 -- Georges Khaznadar <georgesk@debian.org>  Thu, 06 Mar 2025 17:01:11 +0100

rl-accel (0.9.1-1) unstable; urgency=medium

  * New upstream version 0.9.1
  * removed the debian patch which is applied upstream

 -- Georges Khaznadar <georgesk@debian.org>  Wed, 19 Feb 2025 19:04:01 +0100

rl-accel (0.9.0-6) unstable; urgency=medium

  * replaced the build-dependency: libpython3.12-dev -> libpython3-all-dev
    Closes: #1094279

 -- Georges Khaznadar <georgesk@debian.org>  Mon, 27 Jan 2025 10:52:46 +0100

rl-accel (0.9.0-5) unstable; urgency=medium

  * patched setup.py to bypass the obsolete function get_abi_tag
    Closes: #1080137

 -- Georges Khaznadar <georgesk@debian.org>  Sat, 31 Aug 2024 11:49:13 +0200

rl-accel (0.9.0-4) unstable; urgency=medium

  * added export CFLAGS="-I/usr/include/python3.12 -I/usr/include/python3.11"
    in d/rules. Closes: #1074676

 -- Georges Khaznadar <georgesk@debian.org>  Tue, 02 Jul 2024 16:32:46 +0200

rl-accel (0.9.0-3) unstable; urgency=medium

  * new release to upload to unstable
  * added an explicit build-dependency on libpython3.12-dev, which should be
    removed in a near future

 -- Georges Khaznadar <georgesk@debian.org>  Sun, 10 Dec 2023 19:26:10 +0100

rl-accel (0.9.0-2) experimental; urgency=medium

  * added build-dependencies on python3-wheel, libpython3-dev
    (Closes: #1057193)
  * made a short autopkgtest

 -- Georges Khaznadar <georgesk@debian.org>  Fri, 01 Dec 2023 15:03:20 +0100

rl-accel (0.9.0-1) experimental; urgency=medium

  * Initial release. (Closes: #1057193)

 -- Georges Khaznadar <georgesk@debian.org>  Fri, 01 Dec 2023 10:55:52 +0100
